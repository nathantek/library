import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor() {

// <script src="https://www.gstatic.com/firebasejs/5.9.3/firebase.js"></script>
  // Initialize Firebase
  const config = {
    apiKey: 'AIzaSyCfpUeDxzMmdbeyJZ7Wo4NB-7j3VzLX7_U',
    authDomain: 'library-49dad.firebaseapp.com',
    databaseURL: 'https://library-49dad.firebaseio.com',
    projectId: 'library-49dad',
    storageBucket: 'library-49dad.appspot.com',
    messagingSenderId: '809407931462'
  };

  firebase.initializeApp(config);
  }
  title = 'library';
}
